import React, { Component } from 'react';
import '../assets/style/product.css';

class Product extends Component {

    render() {
        const productDetails = this.props.product;
        const description = productDetails.description.slice(0, productDetails.description.indexOf(' ', 150)) + "...";

        return (
            <div className='col'>
                <div className="product card h-100">
                    <img
                        src={productDetails.image}
                        className="product-image card-img-top img-thumbnail"
                        alt="product image"
                    />
                    <div className="card-body">
                        <h6 className="card-title">{productDetails.title}</h6>
                        <p className="card-text">{productDetails.category}</p>
                        <p className="card-text">{description}</p>
                        <p className="card-text">${productDetails.price}</p>
                        <p className="card-text">{productDetails.rating.rate} ({productDetails.rating.count})</p>
                        <a href="#" className="cart-link btn btn-primary">Add to cart</a>
                    </div>
                </div>
            </div>
        );
    }
}

export default Product;