import React, { Component } from 'react';
import axios from 'axios';
import Product from './product';
import Loader from './loader';
import '../assets/style/products.css';

class Products extends Component {
    constructor(props) {
        super(props)
        this.state = {
            products: [],
            isLoading: true,
            errorMessage: null,
        }
    }

    componentDidMount() {
        axios.get(`https://fakestoreapi.com/products`)
            .then(res => {
                const products = res.data;

                this.setState(
                    {
                        products: products,
                        isLoading: false,
                    }
                )
            })
            .catch((error) => {
                let errorMessage = "";

                if (error.request) {
                    // The request was made but no response was received
                    console.log(error.request);
                    errorMessage = "Failed to Load products";
                } else {
                    console.log('Error', error.message);
                    errorMessage = "OOPs something went wrong";
                }
                this.setState(
                    {
                        isLoading: false,
                        errorMessage: errorMessage,
                    }
                )
            });
    }


    render() {
        return (
            (this.state.isLoading)
                ? <Loader />
                : (
                    (!this.state.errorMessage)
                        ? ((this.state.products.length !== 0)
                            ? <div className='container'>
                                <div className='Products row row-cols-1 row-cols-md-2 row-cols-lg-4 g-4'>
                                    {
                                        this.state.products
                                            .map(product =>
                                                <Product
                                                    key={product.id}
                                                    product={product}
                                                />
                                            )
                                    }
                                </div>
                            </div>
                            : <div className='error'>
                                "No product found"
                            </div>)
                        : <div className='error'>
                            {this.state.errorMessage}
                        </div>

                )
        );
    }
}

export default Products;