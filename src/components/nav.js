import React, { Component } from 'react';
import '../assets/style/nav.css';

class Nav extends Component {
    render() {
        return (
            <nav className="navbar bg-body-tertiary">
                <div className="container-fluid">
                    <a className="navbar-brand" href="#">
                        Fake Store
                    </a>
                </div>
            </nav>
        );
    }
}

export default Nav;