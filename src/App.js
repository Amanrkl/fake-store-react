import './App.css';
import Nav from './components/nav';
import Products from './components/products';

function App() {
  return (
    <div className="App">
      <Nav />
      <Products />
    </div>
  );
}

export default App;
